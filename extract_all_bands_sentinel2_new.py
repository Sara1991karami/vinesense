################## Importing sentinel hub module to define the download Bounding Box ##################

from sentinelhub import (
    BBox,
    CRS,
    bbox_to_dimensions,
    to_utm_bbox, 
)

############################# User Input parameters ######################################################

extract_area_WGS84 = [-8.4087748, 40.2205972,-8.4069169,40.2223989]   # Coordinates of the Bounding Box of Interest in WGS84 coordinates (Lower Left corner, Upper right corner)
time_interval = ("2023-05-14", "2023-05-18")     # Time interval to extract images
out_path_folder ="C:\\Mestrado\\Teromovigo\\teste_lixo" # File Save destination folder
Resolution = 10  # Pixel resolution in the image, in float
cloudless_percentage = '100' # sting with the maximum percentage of cloud coverage ('100'= NO CLOUDS)

bbox_WGS84 = BBox(bbox=extract_area_WGS84, crs=CRS.WGS84) # Bouding Box creation with chosen coordinate reference systems (WGS84) 
bbox_UTM = to_utm_bbox(bbox_WGS84) # Coordinate system conversion from Bounding Box CRS to a plane coordinate grid (UTM)
size_bbox = bbox_to_dimensions(bbox_UTM, resolution=Resolution) # Get's Bouding Box pixel count
#print(size_bbox)
bbox_UTM.max_x = bbox_UTM.min_x + (size_bbox[0] * Resolution) # Defines fixed pixel size to be equals to input resolution in x axis
bbox_UTM.max_y = bbox_UTM.min_y + (size_bbox[1] * Resolution) # Defines fixed pixel size to be equals to input resolution in y axis

################### Defines a function to extract satellite images Sentinel-2 given the above mentioned user input #############################

def extract_all_bands(bbox_UTM, time_interval, Resolution, cloudless_percentage, out_path_folder):

############ Imported libraries for data manipulation ###############
    from osgeo import gdal
    import datetime as dt
    import os, shutil, json
    from sentinelhub import SHConfig

    config = SHConfig()

############## Machiel Credentials to automaticaly log to sentinel hub #################
    
    config.instance_id = 'f9ed53ab-a67b-4ab8-b0a5-98164f0bf144'
    config.sh_client_id = '2ebb607e-ebee-4a08-8587-f9c6a5daa037'
    config.sh_client_secret = '-m<doVsE5DE00giRd*I1ZCmiig_@[|yg{n!&RJ)z'


    if not config.sh_client_id or not config.sh_client_secret: # returns a warning if log in fails
        print("Warning! To use Sentinel Hub Catalog API, please provide the credentials (client ID and client secret).")

############ Imports libraries from Sentinel hub module #############
    from sentinelhub import (
        SentinelHubCatalog, 
        MimeType,
        DataCollection,
        SentinelHubDownloadClient,
        SentinelHubRequest,
        filter_times,
        bbox_to_dimensions,   
    )


    catalog = SentinelHubCatalog(config=config) # Login to Sentinel hub given the above mentioned credentials

########### Optional: Bounding box size to variable and Bounding box size print #####################

    #size_bbox = bbox_to_dimensions(bbox_UTM, resolution=Resolution)
    #print(f"Image shape at {Resolution} m resolution: {size_bbox} pixels")

######### Function that allows you to search for all products in the sentinel-2 L2A collection within the above mentioned user date, bouding box and cloud coverage input #####################
    
    search_iterator = catalog.search(
        collection = DataCollection.SENTINEL2_L2A,
        bbox=bbox_UTM,
        time=time_interval,
        filter= "eo:cloud_cover < "+ cloudless_percentage # Filter to set the percentage of clouds in the image
    )

    results_data_in_Time_Interval = list(search_iterator)   # List of all products within the defined time range
    print("Total number of results:", len(results_data_in_Time_Interval))


############ Merges all captured tiles within user input time interval, as follows  ###################

    time_difference = dt.timedelta(hours=1) # Define time interval, in hours, to merge repeated images, and thus obtain a single daily acquisition
    all_timestamps = search_iterator.get_timestamps() # searches all images existante in user date input 
    unique_acquisitions = filter_times(all_timestamps, time_difference) # Temporarily saves crossed refence images by intersecting time diference with all timestamps to a variable
    #print(unique_acquisitions)

##################### Construction of the evalscript to create the image extraction request with the intended sentinel-2 bands, code in java-script #############
    
    evalscript_all_bands = """
        //VERSION=3
        function setup() {
            return {
                input: [{
                    bands: ["B01","B02","B03","B04","B05","B06","B07","B08","B8A","B09","B11","B12"],
                    units: "DN"
                }],
                output: {
                    bands: 12,
                    sampleType: "uint16"
                }
            };
        }

        function evaluatePixel(sample) {
            return [
                sample.B04, 
                sample.B03,
                sample.B02,
                sample.B01,
                sample.B05,
                sample.B06,
                sample.B07,
                sample.B08,
                sample.B08A,
                sample.B9,
                sample.B11,
                sample.B12];
        }
    """
    

##################### Creating a process API request for each acquisition ######################################
################ Source: https://sentinelhub-py.readthedocs.io/en/latest/examples/data_search.html #############

    process_requests = []

    for timestamp in unique_acquisitions:
        request = SentinelHubRequest(
            data_folder = out_path_folder, 
            evalscript=evalscript_all_bands,
            input_data=[
                SentinelHubRequest.input_data(
                    data_collection=DataCollection.SENTINEL2_L2A, # Sentinel Hub API Data Collection
                    time_interval=(timestamp - time_difference, timestamp + time_difference),
                )
            ],
            responses=[SentinelHubRequest.output_response("default", MimeType.TIFF)], # Image file format type
            bbox = bbox_UTM,
            resolution = (Resolution,Resolution),
            config = config,
        )
        process_requests.append(request)

####################### Download the requested data with the previously defined credentials #######################

    client = SentinelHubDownloadClient(config=config)
    download_requests = [request.download_list[0] for request in process_requests]
    client.download(download_requests, show_progress=True)
    
################## Renaming the names of the request and response files and the folder that contains them ##########################
################## TIFF file decompression using GDAL library (DEFLATE to NONE) ##############################

    for folder, _, filenames in os.walk(request.data_folder):

        for filename in filenames:
                ## Rename json file ##
                if filename=="request.json":
                    dados_json = os.path.join(folder, filename)
                    with open(dados_json) as file: # Opens the Json file and saves the image acquisition date
                        dados = json.load(file)
                        data_origem=dados['request']['payload']['input']['data'][0]['dataFilter']['timeRange']['from']
                        data_origem=data_origem.split('T')
                        
                    filename_new= str(data_origem[0]) +'.json'
                    os.rename(os.path.join(folder,filename), os.path.join(folder, filename_new)) # Rename the Json file name

                ## Rename TIFF file ##  
                elif filename=="response.tiff":
                    filename_new= str(data_origem[0]) +'.tiff'
                    translate_options = gdal.TranslateOptions(format="GTiff", options=['COMPRESS=NONE']) # Function to define the type of compression
                    gdal.Translate(os.path.join(folder, filename_new), os.path.join(folder,filename), options=translate_options) # Function to decompress/compress files
                    os.remove(os.path.join(folder,filename)) # Function to remove file with DEFLATE compression

                    #### Rename folder name #########
                    filename_new= os.path.join((os.path.dirname(os.path.abspath(folder))) , 'Sentinel2_' +str(data_origem[0])) # Create rename of folder

                    if (os.path.exists(filename_new)): # Checks if the folder exists and eliminates duplicates
                        shutil.rmtree(folder) 
                    else:
                        os.rename(folder, filename_new) # Change folder name
                else:
                    continue

    return()


########## Use of the definition and the respective parameters ###########

extract_all_bands(bbox_UTM, time_interval, Resolution, cloudless_percentage, out_path_folder)



