
def extract_NDVI(extract_area_WGS84, time_interval, Resolution, cloudless_percentage, out_path_folder):

    #importações de modulos

    import datetime as dt


    import os, shutil, json
    from sentinelhub import SHConfig
    import pdb

    config = SHConfig()
    # Credenciais_Machiel
    config.instance_id = 'f9ed53ab-a67b-4ab8-b0a5-98164f0bf144'
    config.sh_client_id = '2ebb607e-ebee-4a08-8587-f9c6a5daa037'
    config.sh_client_secret = '-m<doVsE5DE00giRd*I1ZCmiig_@[|yg{n!&RJ)z'


    if not config.sh_client_id or not config.sh_client_secret:
        print("Warning! To use Sentinel Hub Catalog API, please provide the credentials (client ID and client secret).")

    from sentinelhub import (
        SentinelHubCatalog, 
        BBox,
        CRS,
        MimeType,
        DataCollection,
        SentinelHubDownloadClient,
        SentinelHubRequest,
        filter_times,
        bbox_to_dimensions,
        to_utm_bbox, 
    )



    catalog = SentinelHubCatalog(config=config)

    # Delimitação da bbox

    bbox_WGS84 = BBox(bbox=extract_area_WGS84, crs=CRS.WGS84)
    print(f"bbox_objs1: {bbox_WGS84}"  ) 
    
    bbox_UTM = to_utm_bbox(bbox_WGS84)

    size_bbox = bbox_to_dimensions(bbox_UTM, resolution=Resolution)
    bbox_UTM.max_x = bbox_UTM.min_x + (size_bbox[0] * Resolution)
    bbox_UTM.max_y = bbox_UTM.min_y + (size_bbox[1] * Resolution)

    print(f"Image shape at {Resolution} m resolution: {size_bbox} pixels")


    #função que permite procurar todos os produtos da coleçao sentinel2_L2A no intervalo de tempo pré definido. 
    search_iterator = catalog.search(
        collection = DataCollection.SENTINEL2_L2A,
        bbox=bbox_UTM,
        time=time_interval,
        filter="eo:cloud_cover <"+ cloudless_percentage,   #filtro de para definir a precentagem de nuvens a apresentar
        #fields={"include": ["id", "properties.datetime", "properties.eo:cloud_cover"], "exclude": []},  #lista de campos a apresentar
    )

    results_data_in_Time_Interval = list(search_iterator)   #lista de todos os produtos dentro do intervalo de tempo definido
    print("Total number of results:", len(results_data_in_Time_Interval))
    #print(results_data_in_Time_Interval)


    # Junção de timestamps quando eles deferem de tempo especifico
    time_difference = dt.timedelta(hours=1) # Definir tempo de diferença dos timestamps

    all_timestamps = search_iterator.get_timestamps()
    unique_acquisitions = filter_times(all_timestamps, time_difference)
    #print(unique_acquisitions)


    #Criação do evalscript para todas as bandas de cada produto
    evalscript_all_bands = """
        //VERSION=3
        function setup() {
            return {
                input: [{
                    bands: ["B01","B02","B03","B04","B05","B06","B07","B08","B8A","B09","B11","B12"],
                    units: "DN"
                }],
                output: {
                    bands: 1,
                    sampleType: "FLOAT32"
                }
            };
        }

        function evaluatePixel(sample) {
            var NDVI = index(sample.B08, sample.B04)
            return [
                NDVI];
        }
    """
    
    #criação de uma solicitação de API de processo para cada aquisição

    process_requests = []

    for timestamp in unique_acquisitions:
        request = SentinelHubRequest(
            data_folder = out_path_folder,
            evalscript=evalscript_all_bands,
            input_data=[
                SentinelHubRequest.input_data(
                    data_collection=DataCollection.SENTINEL2_L2A,
                    time_interval=(timestamp - time_difference, timestamp + time_difference),
                )
            ],
            responses=[SentinelHubRequest.output_response("default", MimeType.TIFF)],
            bbox=bbox_UTM,
            #size=size_bbox,
            resolution = (Resolution,Resolution),
            config=config,
        )
        process_requests.append(request)

    # Download dos dados solicitados
    client = SentinelHubDownloadClient(config=config)

    download_requests = [request.download_list[0] for request in process_requests]
    #print(download_requests)

    data = client.download(download_requests, show_progress=True)

    # print(data[0].shape)

    # renomeação dos nomes dos ficheiros request e response e pasta que os contem
    for folder, _, filenames in os.walk(request.data_folder):

        for filename in filenames: 
                if filename=="request.json":
                    dados_json = os.path.join(folder, filename)
                    with open(dados_json) as file:
                        dados = json.load(file)
                        data_origem=dados['request']['payload']['input']['data'][0]['dataFilter']['timeRange']['from']
                        data_origem=data_origem.split('T')
                        #print(data_origem[0])
                        
                    filename_new= str(data_origem[0]) +'.json'
                    os.rename(os.path.join(folder,filename), os.path.join(folder, filename_new))    
                elif filename=="response.tiff":
                    filename_new= 'NDVI_' + str(data_origem[0]) +'.tiff'
                    os.rename(os.path.join(folder,filename), os.path.join(folder, filename_new))
                    filename_new= folder + '_NDVI_' +str(data_origem[0])
                    if (os.path.exists(filename_new)):
                        shutil.rmtree(folder)
                    else:
                        os.rename(folder, filename_new)
                else:
                    continue
                